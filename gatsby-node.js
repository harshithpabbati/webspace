const { graphqlForPages } = require("./src/scripts/create-pages");

function createIndividualPages(actions, graphql) {
    const { createPage } = actions;

    return Promise.all([
        graphqlForPages(graphql, createPage),
    ])
}

exports.createPages = ({ graphql, actions }) => {
    return createIndividualPages(actions, graphql);
};