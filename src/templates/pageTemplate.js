import React from "react";
import {graphql, Link} from "gatsby"
import Layout from "../components/layout";
import SEO from "../components/seo";

function getFormattedDate() {
    let date = new Date();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;
    return day + "-" + month + "-" + date.getFullYear() + " " +  hour + ":" + min + ":" + sec;
}

function bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return 'n/a';
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
    if (i === 0) return `${bytes} ${sizes[i]})`;
    return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`;
}

const PageTemplate = ({ data: { contentYaml } }) => {
    const files = contentYaml.files;
    return(
        <Layout>
            <SEO title={contentYaml.username} />
            <h1>Index of /~{contentYaml.username}/</h1>
            <hr />
            <pre className="row m-0">
                <div className="col-md-6 col-sm-12">
                    {files.map(file => (
                        <div className="row p-1">
                            <div className="col-md-4">
                                <a href={file.publicURL} target="_blank">
                                    {file.base}
                                </a>
                            </div>
                            <div className="col-md-6">
                                {getFormattedDate(file.birthTime)}
                            </div>
                            <div className="col-md-2">
                                {bytesToSize(file.size)}
                            </div>
                        </div>
                    ))}
                </div>
            </pre>
        </Layout>
    )
};

export default PageTemplate

export const pageQuery = graphql`
    query($username: String!) {
        contentYaml(username: { eq: $username }){
            username
            files{
                base
                publicURL
                birthTime
                size
            }
        }
    }
`;