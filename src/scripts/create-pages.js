const path = require("path");

function createPages(result, createPage) {
    const PageTemplate = path.resolve(`src/templates/pageTemplate.js`);
    const pages = result.data.allContentYaml.edges;
    pages.forEach(({ node }) => {
        createPage({
            path: "~" + node.username,
            component: PageTemplate,
            context: {
                username: node.username,
            },
        })
    })
}

function graphqlForPages(graphql, createPage) {
    return graphql(`
    {
      allContentYaml{
        edges{
          node{
            username
          }
        }
      }
    }
  `).then(result => {
        if (result.errors) {
            throw result.errors
        }
        createPages(result, createPage)
    })
}

exports.graphqlForPages = graphqlForPages;