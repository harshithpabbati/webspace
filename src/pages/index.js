import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import "../styles/style.sass"

const IndexPage = () => {
    return (
        <Layout>
            <SEO title="Home"/>
            <h1>Index</h1>
            <p>Welcome to amFOSS Webspace</p>
        </Layout>
    )
};

export default IndexPage
